## greenfield starts here
create a directory [MyLegoCatalog]

everythink in [is pointed to this exsample feel free to change]

change into the new directory and create some more.
[mkdir csv]
[mkdir rawImages]

## befor start download some assets first

Start with bricks and colors your are want to catalog.
[CSV files and Images archives](https://rebrickable.com/downloads/)

## do some research

We want to use the CVS files to pick the brick information and display it to a web frontend.
In that case we follow the fairytale and use JavaScript in Frontend and Backend for better interoperability.

So type *node csv database* in your browser, in my case there are tow npm projects on the top of the resulte.
Quick check of both [csv-database](https://www.npmjs.com/package/csv-database) and [csv-db](https://www.npmjs.com/package/csv-db) the csv-database make the race. It supports async/await out of the box and the last publish date is one month newer.

## create a git repository (aka GitHub.com or GitLab.com)

Go to your favorite service and create a empty project.

## lets start with npm (generate package.json)

npm init

*fill out the questions like this*

```json
{
  "name": "mylegocatalog",
  "version": "0.0.1",
  "description": "My List of Lego Bricks or some i like to have",
  "main": "app.js",
  "scripts": {
    "test": "npm test"
  },
  "repository": {
    "type": "git",
    "url": "git+ssh://git@gitlab.com/dexta/mylegocatalog.git"
  },
  "keywords": [
    "Lego",
    "Bricks",
    "Catalog"
  ],
  "author": "dexta.de",
  "license": "GPL-3.0",
  "bugs": {
    "url": "https://gitlab.com/dexta/mylegocatalog/issues"
  },
  "homepage": "https://gitlab.com/dexta/mylegocatalog#README"
}
```

## continue with git (add files,...)

Before type *git init* we create a file called **.gitignore** to tell git not include some files/folders.

```ini
csv/
rawImages/
```

Now we follow the friendly instruction 

```bash
git init
git remote add origin git@gitlab.com:dexta/mylegocatalog.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

## now do some house cleaning